package com.oredata.nifi.processors;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.behavior.SupportsBatching;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.ValidationContext;
import org.apache.nifi.components.ValidationResult;
import org.apache.nifi.expression.AttributeValueDecorator;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.DataUnit;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.io.StreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.stream.io.StreamUtils;
import org.apache.nifi.util.StopWatch;

@EventDriven
@SideEffectFree
@SupportsBatching
@InputRequirement(Requirement.INPUT_REQUIRED)
@Tags({ "Text", "Regular Expression", "Update", "Change", "Replace", "Modify", "Regex", "Mapping" })
@CapabilityDescription("Updates the content of a FlowFile by evaluating a Regular Expression against it and replacing the section of the content that "
		+ "matches the Regular Expression with some alternate value provided in a mapping file.")
public class TrafficDRParser extends AbstractProcessor {

	public static HashMap<String, String> eventTypeToDirectionMap = new HashMap<String, String>();
	public static HashMap<String, String> eventTypeToEventTypeMap = new HashMap<String, String>();

	static {
		eventTypeToDirectionMap.put("001", "2");
		eventTypeToDirectionMap.put("029", "3");
		eventTypeToDirectionMap.put("031", "2");
		eventTypeToDirectionMap.put("002", "1");
		eventTypeToDirectionMap.put("030", "1");
		eventTypeToDirectionMap.put("50", "4");
		eventTypeToDirectionMap.put("30", "1");
		eventTypeToDirectionMap.put("20", "2");

		eventTypeToEventTypeMap.put("001", "D");
		eventTypeToEventTypeMap.put("029", "D");
		eventTypeToEventTypeMap.put("031", "E");
		eventTypeToEventTypeMap.put("002", "D");
		eventTypeToEventTypeMap.put("030", "E");
		eventTypeToEventTypeMap.put("026", "D");
	}

	public static final PropertyDescriptor REGEX = new PropertyDescriptor.Builder().name("Regular Expression")
			.description("The Regular Expression to search for in the FlowFile content").required(true)
			.addValidator(StandardValidators.createRegexValidator(0, Integer.MAX_VALUE, true))
			.expressionLanguageSupported(true).defaultValue("\\S+").build();

	public static final PropertyDescriptor MAPPING_FILE = new PropertyDescriptor.Builder().name("Mapping File")
			.description("The name of the file (including the full path) containing the Mappings.")
			.addValidator(StandardValidators.FILE_EXISTS_VALIDATOR).build();
	public static final PropertyDescriptor MAPPING_FILE_REFRESH_INTERVAL = new PropertyDescriptor.Builder()
			.name("Mapping File Refresh Interval")
			.description(
					"The polling interval in seconds to check for updates to the mapping file. The default is 60s.")
			.addValidator(StandardValidators.TIME_PERIOD_VALIDATOR).defaultValue("60s").build();
	public static final PropertyDescriptor CHARACTER_SET = new PropertyDescriptor.Builder().name("Character Set")
			.description("The Character Set in which the file is encoded").required(true)
			.addValidator(StandardValidators.CHARACTER_SET_VALIDATOR).defaultValue("UTF-8").build();
	public static final PropertyDescriptor MAX_BUFFER_SIZE = new PropertyDescriptor.Builder()
			.name("Maximum Buffer Size")
			.description(
					"Specifies the maximum amount of data to buffer (per file) in order to apply the regular expressions. If a FlowFile is larger "
							+ "than this value, the FlowFile will be routed to 'failure'")
			.required(true).addValidator(StandardValidators.DATA_SIZE_VALIDATOR).defaultValue("1 MB").build();

	public static final PropertyDescriptor MOBILE_NUMBER_LENGTH = new PropertyDescriptor.Builder()
			.name("Local Mobile Number Length").required(true).defaultValue("9")
			.addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();
	public static final PropertyDescriptor DATE_SUBSTRING_LENGTH = new PropertyDescriptor.Builder()
			.name("Date Substring Length").required(true).defaultValue("8")
			.addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor EVENT_DATE = new PropertyDescriptor.Builder().name("Event Date Group")
			.required(true).defaultValue("7").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor DIRECTION = new PropertyDescriptor.Builder().name("Direction Group")
			.required(true).defaultValue("1").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor BPARTY = new PropertyDescriptor.Builder().name("B Party Index")
			.required(true).defaultValue("6").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor APARTY = new PropertyDescriptor.Builder().name("A Party Index")
			.required(true).defaultValue("3").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor EVENT_TYPE = new PropertyDescriptor.Builder().name("Event Type Index")
			.required(true).defaultValue("12").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor COUNTRY_CODE = new PropertyDescriptor.Builder().name("BParty Country Code")
			.required(true).defaultValue("13").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor DATE_PARTITION = new PropertyDescriptor.Builder().name("Date Partition")
			.required(true).defaultValue("13").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor LOCALITY = new PropertyDescriptor.Builder().name("Locality").required(true)
			.defaultValue("5").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor REPLACED_TEXT = new PropertyDescriptor.Builder().name("Replaced Text")
			.required(true).defaultValue("$1").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor DATA_UNIT = new PropertyDescriptor.Builder().name("Data Unit").required(true)
			.defaultValue("KB").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final PropertyDescriptor FILE_NAME = new PropertyDescriptor.Builder().name("File Name").required(true)
			.defaultValue("-1").addValidator(StandardValidators.NON_EMPTY_VALIDATOR).build();

	public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
			.description(
					"FlowFiles that have been successfully updated are routed to this relationship, as well as FlowFiles whose content does not match the given Regular Expression")
			.build();
	public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
			.description("FlowFiles that could not be updated are routed to this relationship").build();

	private final Pattern backReferencePattern = Pattern.compile("[^\\\\]\\$(\\d+)");

	private List<PropertyDescriptor> properties;
	private Set<Relationship> relationships;

	@Override
	protected Collection<ValidationResult> customValidate(final ValidationContext context) {
		final List<ValidationResult> errors = new ArrayList<>(super.customValidate(context));

		// final String regexValue =
		// context.getProperty(REGEX).evaluateAttributeExpressions().getValue();
		// final int numCapturingGroups =
		// Pattern.compile(regexValue).matcher("").groupCount();
		// final int groupToMatch =
		// context.getProperty(MATCHING_GROUP_FOR_LOOKUP_KEY).evaluateAttributeExpressions().asInteger();
		//
		// if (groupToMatch > numCapturingGroups) {
		// errors.add(
		// new ValidationResult.Builder()
		// .subject("Insufficient Matching Groups")
		// .valid(false)
		// .explanation("The specified matching group does not exist for the
		// regular expression provided")
		// .build());
		// }
		return errors;
	}

	@Override
	protected void init(final ProcessorInitializationContext context) {
		final List<PropertyDescriptor> properties = new ArrayList<>();
		properties.add(REGEX);
		properties.add(REPLACED_TEXT);
		properties.add(MAPPING_FILE);
		properties.add(MAPPING_FILE_REFRESH_INTERVAL);
		properties.add(CHARACTER_SET);
		properties.add(MAX_BUFFER_SIZE);
		properties.add(MOBILE_NUMBER_LENGTH);
		properties.add(DATE_SUBSTRING_LENGTH);
		properties.add(DATA_UNIT);

		properties.add(APARTY);
		properties.add(BPARTY);
		properties.add(DIRECTION);
		properties.add(EVENT_TYPE);
		properties.add(EVENT_DATE);
		properties.add(LOCALITY);
		properties.add(COUNTRY_CODE);
		properties.add(DATE_PARTITION);
		properties.add(FILE_NAME);

		this.properties = Collections.unmodifiableList(properties);

		final Set<Relationship> relationships = new HashSet<>();
		relationships.add(REL_SUCCESS);
		relationships.add(REL_FAILURE);
		this.relationships = Collections.unmodifiableSet(relationships);
	}

	@Override
	protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
		return properties;
	}

	@Override
	public Set<Relationship> getRelationships() {
		return relationships;
	}

	@Override
	public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
		final List<FlowFile> flowFiles = session.get(1);
		if (flowFiles.isEmpty()) {
			return;
		}

		final int maxBufferSize = context.getProperty(MAX_BUFFER_SIZE).asDataSize(DataUnit.B).intValue();

		// final ProcessorLog logger = getLogger();

		final Charset charset = Charset.forName(context.getProperty(CHARACTER_SET).getValue());
		final byte[] buffer = new byte[context.getProperty(MAX_BUFFER_SIZE).asDataSize(DataUnit.B).intValue()];
		for (FlowFile flowFile : flowFiles) {
			if (flowFile.getSize() > maxBufferSize) {
				// logger.info("Removed from list - above buffer size :" +
				// flowFile.getAttribute(CoreAttributes.FILENAME.key()));
				session.transfer(flowFile, REL_FAILURE);
				continue;
			}

			final IntegerHolder bufferedByteCount = new IntegerHolder(0);
			session.read(flowFile, new InputStreamCallback() {
				@Override
				public void process(final InputStream in) throws IOException {
					bufferedByteCount.set(StreamUtils.fillBuffer(in, buffer, false));
				}
			});

			final String contentString = new String(buffer, 0, bufferedByteCount.get(), charset);

			final StopWatch stopWatch = new StopWatch(true);

			try {
				flowFile = session.write(flowFile,
						new ReplaceTextCallback(context, flowFile, contentString, maxBufferSize));
			} catch (Exception e) {
				// logger.error(e.getMessage());
				session.transfer(flowFile, REL_FAILURE);
				return;
			}

			// logger.debug("Transferred {} to 'success'", new
			// Object[]{flowFile});
			session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getElapsed(TimeUnit.MILLISECONDS));
			session.transfer(flowFile, REL_SUCCESS);
		}
	}

	private final class ReplaceTextCallback implements StreamCallback {

		private final Charset charset;
		private final String regex;
		private final String groupToMatch;

		private final int mobileNumberlength;
		private final int dateSubstringLength;
		private final int bParty;
		private final int aParty;
		private final int eventDate;
		private final int direction;
		private final int eventType;
		private final int countryCode;
		private final int datePartition;
		private final int locality;
		private final String dataUnit;
		private final int fileName;

		private final String content;

		final Pattern regexPattern;

		private final FlowFile flowFile;

		private final AttributeValueDecorator quotedAttributeDecorator = new AttributeValueDecorator() {
			@Override
			public String decorate(final String attributeValue) {
				return Pattern.quote(attributeValue);
			}
		};

		private ReplaceTextCallback(ProcessContext context, FlowFile flowFile, String content, int maxBufferSize) {
			this.regex = context.getProperty(REGEX).evaluateAttributeExpressions(flowFile, quotedAttributeDecorator)
					.getValue();

			this.regexPattern = Pattern.compile(regex);

			this.content = content;

			this.flowFile = flowFile;

			this.mobileNumberlength = context.getProperty(MOBILE_NUMBER_LENGTH).evaluateAttributeExpressions()
					.asInteger();
			this.dateSubstringLength = context.getProperty(DATE_SUBSTRING_LENGTH).evaluateAttributeExpressions()
					.asInteger();
			this.aParty = context.getProperty(APARTY).evaluateAttributeExpressions().asInteger();
			this.bParty = context.getProperty(BPARTY).evaluateAttributeExpressions().asInteger();
			this.eventDate = context.getProperty(EVENT_DATE).evaluateAttributeExpressions().asInteger();
			this.direction = context.getProperty(DIRECTION).evaluateAttributeExpressions().asInteger();
			this.eventType = context.getProperty(EVENT_TYPE).evaluateAttributeExpressions().asInteger();
			this.countryCode = context.getProperty(COUNTRY_CODE).evaluateAttributeExpressions().asInteger();
			this.datePartition = context.getProperty(DATE_PARTITION).evaluateAttributeExpressions().asInteger();
			this.locality = context.getProperty(LOCALITY).evaluateAttributeExpressions().asInteger();
			this.dataUnit = context.getProperty(DATA_UNIT).evaluateAttributeExpressions().getValue();
			this.fileName = context.getProperty(FILE_NAME).evaluateAttributeExpressions().asInteger();

			this.charset = Charset.forName(context.getProperty(CHARACTER_SET).getValue());

			this.groupToMatch = context.getProperty(REPLACED_TEXT).evaluateAttributeExpressions().getValue();
		}

		@Override
		public void process(final InputStream in, final OutputStream out) throws IOException {
			final ComponentLog logger = getLogger();

			try {
				StringTokenizer st = new StringTokenizer(content, "\n|\n\r");

				while (st.hasMoreTokens()) {
					String oneLine = st.nextToken();

					// logger.debug("Content: " + oneLine);

					try {
						String fileNameAtt = flowFile.getAttribute("filename");
						final Matcher matcher = regexPattern.matcher(oneLine);
						String[] fields = new String[30];
						if (matcher.find()) {
							for (int i = 1; i <= matcher.groupCount(); i++) {
								final String groupValue = matcher.group(i);
								fields[i] = groupValue.trim();
								// logger.debug("i: " + i + "-" + groupValue);
							}

							if (direction != 0) {
								String directionValue = fields[direction];
								fields[direction] = eventTypeToDirectionMap.get(directionValue);
								if (fields[eventType] == null || fields[eventType].isEmpty()) {
									fields[eventType] = eventTypeToEventTypeMap.get(directionValue);
								}
							}

							if (eventDate != 0) {
								String eventDateValue = fields[eventDate];
								if (eventDateValue != null && !eventDateValue.isEmpty()
										&& eventDateValue.length() > dateSubstringLength) {
									fields[datePartition] = eventDateValue.substring(0, dateSubstringLength);
								}
							}

							if (bParty != 0 && fields[bParty] != null && !fields[bParty].isEmpty()
									&& fields[locality] != null
									&& (fields[locality].equals("01") || fields[locality].equals("1"))) {
								if (fields[bParty].startsWith("966") || fields[bParty].startsWith("800")) {
									fields[locality] = "02";
									fields[countryCode] = "";
								} else {
									int len = fields[bParty].length();

									if (len > mobileNumberlength && StringUtils.isNumeric(fields[bParty])) {
										fields[countryCode] = fields[bParty].substring(0, len - mobileNumberlength);
									} else {
										fields[countryCode] = "";
									}
								}
							} else {
								fields[countryCode] = "";
							}

							if (fields[aParty] != null) {
								if (fields[aParty].startsWith("0")) {
									fields[aParty] = fields[aParty].substring(1);
								} else if (fields[aParty].startsWith("966")) {
									fields[aParty] = fields[aParty].substring(3);
								}
							}

							if (fields[bParty] != null) {
								if (fields[bParty].startsWith("0")) {
									fields[bParty] = fields[bParty].substring(1);
								} else if (fields[bParty].startsWith("966")) {
									fields[bParty] = fields[bParty].substring(3);
								}
							}

							fields[fileName] = fileNameAtt;

							StringBuffer sb = new StringBuffer();
							Pattern p = Pattern.compile("\\$[0-9]+");
							Matcher m = p.matcher(groupToMatch);

							while (m.find()) {
								String newValue = fields[Integer.parseInt(m.group().substring(1))];
								if (newValue != null) {
									m.appendReplacement(sb, newValue);
								} else {
									m.appendReplacement(sb, "");
								}
							}
							m.appendTail(sb);

							String evaluateForMathExpressions = new String(sb.toString());

							sb = new StringBuffer();
							p = Pattern.compile("[^,]+");
							m = p.matcher(evaluateForMathExpressions);

							while (m.find()) {
								String[] splitted = m.group().split("\\+|\\-|\\*|\\/");
								String expression = m.group();
								if (expression != null && !expression.isEmpty()) {
									expression = expression.replaceAll("\\++", "\\+");
									expression = expression.endsWith("+")
											? expression.substring(0, expression.length() - 1) : expression;
								}

								if (splitted != null && splitted.length > 1) {
									try {
										String evaluated = eval(expression).toString();
										if (evaluated != null) {
											m.appendReplacement(sb, evaluated);
										}
										if (evaluated != null && !evaluated.isEmpty()) {
											if (dataUnit.equals("B")) {
												evaluated = Integer.parseInt(evaluated) / 1024 + "";
											} else if (dataUnit.equals("KB")) {
												// Do nothing
											} else if (dataUnit.equals("MB")) {
												evaluated = Integer.parseInt(evaluated) * 1024 + "";
											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								} else if (splitted != null && splitted.length == 1) {
									m.appendReplacement(sb, expression);
								} else if (splitted != null && splitted.length == 0) {
									m.appendReplacement(sb, "");
								}
							}
							m.appendTail(sb);

							sb.append("\n");

							out.write(sb.toString().getBytes(charset));
						}
					} catch (Exception e) {
						logger.error("Error while parsing file", e);
					}
				}
			} catch (Exception e) {
				logger.error("Error while parsing file", e);
			}
		}
	}

	public static final Double eval(final String str) {
		return new Object() {
			int pos = -1, ch;

			void nextChar() {
				ch = (++pos < str.length()) ? str.charAt(pos) : -1;
			}

			boolean eat(int charToEat) {
				while (ch == ' ')
					nextChar();
				if (ch == charToEat) {
					nextChar();
					return true;
				}
				return false;
			}

			double parse() {
				nextChar();
				double x = parseExpression();
				if (pos < str.length())
					throw new RuntimeException("Unexpected: " + (char) ch);
				return x;
			}

			// Grammar:
			// expression = term | expression `+` term | expression `-` term
			// term = factor | term `*` factor | term `/` factor
			// factor = `+` factor | `-` factor | `(` expression `)`
			// | number | functionName factor | factor `^` factor

			double parseExpression() {
				double x = parseTerm();
				for (;;) {
					if (eat('+'))
						x += parseTerm(); // addition
					else if (eat('-'))
						x -= parseTerm(); // subtraction
					else
						return x;
				}
			}

			double parseTerm() {
				double x = parseFactor();
				for (;;) {
					if (eat('*'))
						x *= parseFactor(); // multiplication
					else if (eat('/'))
						x /= parseFactor(); // division
					else
						return x;
				}
			}

			double parseFactor() {
				if (eat('+'))
					return parseFactor(); // unary plus
				if (eat('-'))
					return -parseFactor(); // unary minus

				double x;
				int startPos = this.pos;
				if (eat('(')) { // parentheses
					x = parseExpression();
					eat(')');
				} else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
					while ((ch >= '0' && ch <= '9') || ch == '.')
						nextChar();
					x = Double.parseDouble(str.substring(startPos, this.pos));
				} else if (ch >= 'a' && ch <= 'z') { // functions
					while (ch >= 'a' && ch <= 'z')
						nextChar();
					String func = str.substring(startPos, this.pos);
					x = parseFactor();
					if (func.equals("sqrt"))
						x = Math.sqrt(x);
					else if (func.equals("sin"))
						x = Math.sin(Math.toRadians(x));
					else if (func.equals("cos"))
						x = Math.cos(Math.toRadians(x));
					else if (func.equals("tan"))
						x = Math.tan(Math.toRadians(x));
					else
						throw new RuntimeException("Unknown function: " + func);
				} else {
					throw new RuntimeException("Unexpected: " + (char) ch);
				}

				if (eat('^'))
					x = Math.pow(x, parseFactor()); // exponentiation

				return x;
			}
		}.parse();
	}
}