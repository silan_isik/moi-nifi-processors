package com.oredata.nifi.processors;

public class IntegerHolder {
	private int value;

	public IntegerHolder(int value) {
		this.value = value;
	}

	public int get() {
		return value;
	}

	public void set(int value) {
		this.value = value;
	}
}
