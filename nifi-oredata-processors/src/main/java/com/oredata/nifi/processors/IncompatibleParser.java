package com.oredata.nifi.processors;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.behavior.SupportsBatching;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.ValidationContext;
import org.apache.nifi.components.ValidationResult;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.flowfile.attributes.CoreAttributes;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.DataUnit;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.io.StreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.stream.io.StreamUtils;
import org.apache.nifi.util.StopWatch;

@EventDriven
@SideEffectFree
@SupportsBatching
@InputRequirement(Requirement.INPUT_REQUIRED)
@Tags({ "Text", "Regular Expression", "Update", "Change", "Replace", "Modify", "Regex", "Mapping" })
@CapabilityDescription("Updates the content of a FlowFile by evaluating a Regular Expression against it and replacing the section of the content that "
		+ "matches the Regular Expression with some alternate value provided in a mapping file.")
public class IncompatibleParser extends AbstractProcessor {

	public static final PropertyDescriptor MAX_BUFFER_SIZE = new PropertyDescriptor.Builder()
			.name("Maximum Buffer Size")
			.description(
					"Specifies the maximum amount of data to buffer (per file) in order to apply the regular expressions. If a FlowFile is larger "
							+ "than this value, the FlowFile will be routed to 'failure'")
			.required(true).addValidator(StandardValidators.DATA_SIZE_VALIDATOR).defaultValue("1 MB").build();

	public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
			.description(
					"FlowFiles that have been successfully updated are routed to this relationship, as well as FlowFiles whose content does not match the given Regular Expression")
			.build();
	public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
			.description("FlowFiles that could not be updated are routed to this relationship").build();

	private List<PropertyDescriptor> properties;
	private Set<Relationship> relationships;

	@Override
	protected Collection<ValidationResult> customValidate(final ValidationContext context) {
		final List<ValidationResult> errors = new ArrayList<>(super.customValidate(context));
		return errors;
	}

	@Override
	protected void init(final ProcessorInitializationContext context) {
		final List<PropertyDescriptor> properties = new ArrayList<>();
		properties.add(MAX_BUFFER_SIZE);
		this.properties = Collections.unmodifiableList(properties);
		final Set<Relationship> relationships = new HashSet<>();
		relationships.add(REL_SUCCESS);
		relationships.add(REL_FAILURE);
		this.relationships = Collections.unmodifiableSet(relationships);
	}

	@Override
	protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
		return properties;
	}

	@Override
	public Set<Relationship> getRelationships() {
		return relationships;
	}

	@Override
	public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
		final List<FlowFile> flowFiles = session.get(1);
		if (flowFiles.isEmpty()) {
			return;
		}

		final ComponentLog logger = getLogger();

		final int maxBufferSize = context.getProperty(MAX_BUFFER_SIZE).asDataSize(DataUnit.B).intValue();
		final Charset charset = Charset.forName("UTF-8");
		final byte[] buffer = new byte[maxBufferSize];
		for (FlowFile flowFile : flowFiles) {
			if (flowFile.getSize() > maxBufferSize) {
				logger.info("Removed from list - above buffer size :"
						+ flowFile.getAttribute(CoreAttributes.FILENAME.key()));
				session.transfer(flowFile, REL_FAILURE);
				continue;
			}

			final IntegerHolder bufferedByteCount = new IntegerHolder(0);
			session.read(flowFile, new InputStreamCallback() {
				@Override
				public void process(final InputStream in) throws IOException {
					bufferedByteCount.set(StreamUtils.fillBuffer(in, buffer, false));
				}
			});

			final String contentString = new String(buffer, 0, bufferedByteCount.get(), charset);

			final StopWatch stopWatch = new StopWatch(true);

			try {
				flowFile = session.write(flowFile,
						new ReplaceTextCallback(context, flowFile, contentString, maxBufferSize));
			} catch (Exception e) {
				logger.error(e.getMessage());
				session.transfer(flowFile, REL_FAILURE);
				return;
			}

			logger.debug("Transferred {} to 'success'", new Object[] { flowFile });
			session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getElapsed(TimeUnit.MILLISECONDS));
			session.transfer(flowFile, REL_SUCCESS);
		}
	}

	private final class ReplaceTextCallback implements StreamCallback {

		private final String content;
		private final FlowFile flowFile;

		private ReplaceTextCallback(ProcessContext context, FlowFile flowFile, String content, int maxBufferSize) {
			this.content = content;
			this.flowFile = flowFile;
		}

		@Override
		public void process(final InputStream in, final OutputStream out) throws IOException {
			final ComponentLog logger = getLogger();
			try {
				String fileNameAtt = flowFile.getAttribute("filename");
				StringTokenizer st = new StringTokenizer(content, "\n|\n\r");
				while (st.hasMoreTokens()) {
					String oneLine = st.nextToken();

					StringBuilder sb = new StringBuilder();

					String str = oneLine.replace(", ,", ",,");
					String[] splitArr = str.split(",", -1);

					int length = splitArr.length;

					if (length >= 24) {
						sb.append(str);
					} else {
						String[] result = new String[25];
						for (int i = 0; i < result.length; i++) {
							result[i] = "";
						}

						result[1] = fileNameAtt;
						int i = 2;
						for (int j = 0; j < 21; j++) {
							if (i == 20) {
								i++;
								j--;
							} else {
								result[i++] = splitArr[j];
							}
						}
						for (String string : result) {
							sb.append(string).append(",");
						}
					}
					sb.append("\n");

					out.write(sb.toString().getBytes("UTF-8"));
				}
			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				logger.error(sw.getBuffer().toString());
			}
		}
	}
	
	public static void main(String[] args) {
		double num = 256017524;
		System.out.println(String.format("%.3f", num));
	}
}